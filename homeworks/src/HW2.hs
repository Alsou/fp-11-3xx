-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True


data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа
            deriving (Show)
eval :: Term -> Int
eval (Mult x y) = (eval x) * (eval y)
eval (Add x y)  = (eval x) + (eval y)
eval (Sub x y)  = (eval x) - (eval y)
eval (Const x)  = x

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3

simplify :: Term -> Term
simplify (Const x) = Const x
simplify (Mult (Add x y) z) = simplify (Add (Mult x z) (Mult y z))
simplify (Mult (Sub x y) z) = simplify (Sub (Mult x z) (Mult y z))
simplify (Mult z (Add x y)) = simplify (Add (Mult x z) (Mult y z))
simplify (Mult z (Sub x y)) = simplify (Sub (Mult x z) (Mult y z))
simplify (Mult x y) = ( Mult (simplify x) (simplify y) )
simplify (Add x y)  = Add  (simplify x) (simplify y)
simplify (Sub x y)  = Sub  (simplify x) (simplify y)
