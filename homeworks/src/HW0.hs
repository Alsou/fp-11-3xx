module HW0 where

-- |The 'hw0_0' function appends given string to the "Hello, " string
-- E.g.
--
-- >>> hw0_0 "Alice"
-- Hello, Alice
hw0_0 :: String -> String
hw0_0 user = "Hello, " ++ user
