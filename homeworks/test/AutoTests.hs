module AutoTests where

-- Do not change! It will be overwritten!
-- Use MyTests.hs to define your tests

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit

import Lib

exampleTests :: [TestTree]
exampleTests =
  [ testGroup "HW 0"
    [ testGroup "HW 0.0"
      [ testCase "Works for Alice" $ hw0_0 "Alice" @?= "Hello, Alice"
      , testCase "Works for Bob"   $ hw0_0 "Bob"   @?= "Hello, Bob"
      ]
    ]
  , testGroup "HW 1"
    [ testGroup "HW 1.1"
      [ testCase "Works for 2 and 3"    $ hw1_1 2 3    @?= 5
      , testCase "Works for 3 and 1000" $ hw1_1 3 1000 @?= 1003
      ]
    , testGroup "HW 1.2"
      [ testCase "Works on 2" $ hw1_2 2 @?= 1.25 ]
    , testGroup "n!!"
      [ testCase "3!!" $ fact2 3 @?= 3
      , testCase "4!!" $ fact2 4 @?= 8
      ]
    , testGroup "isPrime"
      [ testCase "isPrime 13" $ isPrime 13 @?= True
      , testCase "isPrime 12" $ isPrime 12 @?= False
      , testCase "isPrime 2"  $ isPrime 2  @?= True
      , testCase "isPrime 1"  $ isPrime 1  @?= False
      ]
    , testGroup "primeSum"
      [ testCase "primeSum 2 3" $ primeSum 2 3 @?= 5
      , testCase "primeSum 3 2" $ primeSum 3 2 @?= 0
      , testCase "primeSum 2 2" $ primeSum 2 2 @?= 2
      , testCase "primeSum 1 10" $ primeSum 1 10 @?= 17
      ]
    ]
  ]

autoTests :: [TestTree]
autoTests = [ ]

