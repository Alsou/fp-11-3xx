data Term = Var String 
          | Lambda String Term 
          | Func Term Term
          deriving(Eq)

instance Show Term where
    show (Var v) = v
    show (Func t1 t2) = "(" ++ (show t1) ++ " " ++ (show t2) ++ ")"
    show (Lambda x t) = "(\\" ++ x ++ "." ++ (show t) ++ ")" 

renameFree :: [String] -> Term -> Term
renameFree lst (Lambda x term) | elem x lst = Lambda x (renameFree (filter (\el -> el /= x) lst) term)
                               | otherwise = Lambda x (renameFree lst term)
renameFree lst (Func t1 t2) = Func (renameFree lst t1) (renameFree lst t2)
renameFree lst (Var v) | elem v lst = Var (rename lst v 0)
                       | otherwise = Var v

rename :: [String] -> String -> Int -> String
rename list v n | elem (v ++ (show n)) list = rename list v (n+1)
                | otherwise = v ++ (show n)


apply :: String -> [String] -> Term -> Term -> Term
apply var lst (Lambda x term) applied | x /= var = Lambda x (apply var (x:lst) term applied)
                                      | otherwise = Lambda x term
apply var lst (Func term1 term2) applied = Func (apply var lst term1 applied) (apply var lst term2 applied)
apply var lst (Var v) applied | v == var = renameFree lst applied
                              | otherwise = Var v


eval1' :: Term -> Term
eval1' (Func (Var v) t) = (Func (Var v) (eval1' t))
eval1' (Func (Lambda x term) t) = apply x [] term t
eval1' (Func (Func term1 term2) t) = Func (eval1' (Func term1 term2)) t
eval1' term = term


eval' :: Term -> Term
eval' (Func (Var v) t) = (Func (Var v) (eval' t))
eval' (Func (Lambda x term) t) = eval' (apply x [] term t)
eval' (Func (Func term1 term2) t) = eval' (Func (eval' (Func term1 term2)) t)
eval' term = term



--((\x.\y.x) z) ((\x. (x x))(\x. (x x)))
ex1 = Func (Func (Lambda "x" (Lambda "y" (Var "x"))) (Var "z")) (Func (Lambda "x" (Func (Var "x") (Var "x"))) (Lambda "x" (Func (Var "x") (Var "x"))))
--(\x.\y y) y
ex2 = Func (Lambda "x" (Lambda "y" (Var "y"))) (Var "y") 
----(\x.\y x) (\z.z)
ex3 = Func (Lambda "x" (Lambda "y" (Var "x"))) (Lambda "z" (Var "z"))
--(\x.\y x) y 
ex4 = Func (Lambda "x" (Lambda "y" (Var "x"))) (Var "y") 
--(\x.\y y) z
ex5 = Func (Lambda "x" (Lambda "y" (Var "y"))) (Var "z")

--(λx.x) ((λx.x) (λz. (λx.x) z))
ex6 = Func (Lambda "x" (Var "x")) (Func (Lambda "x" (Var "x")) (Lambda "z" (Func (Lambda "x" (Var "x")) (Var "z"))))


--(\m. \n. \s. \z. m (n s) z) (\s. \z. s (s z)) (\s. \z. s z) S Z
f1 = Lambda "m" (Lambda "n" (Lambda "s" (Lambda "z" (Func (Func (Var "m") (Func (Var "n") (Var "s"))) (Var "z")))))
f2 = Lambda "s" (Lambda "z" (Func (Var "s") (Func (Var "s") (Var "z"))))
f3 = Lambda "s" (Lambda "z" (Func (Var "s") (Var "z")))
ex7 = Func (Func (Func (Func f1 f2) f3) (Var "S")) (Var "Z")